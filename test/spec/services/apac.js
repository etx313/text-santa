'use strict';

describe('Service: Apac', function () {

  // load the service's module
  beforeEach(module('santaApp'));

  // instantiate service
  var Apac;
  beforeEach(inject(function (_Apac_) {
    Apac = _Apac_;
  }));

  it('should do something', function () {
    expect(!!Apac).toBe(true);
  });

});
