
/*
*   Users API
*/

var monk = require('monk'),
    db =  monk('localhost:27017/santa'),
    collection = db.get('users');


exports.index = function(req, res){
  
  if( req.session.user !== null )
    res.json(req.session.user);
    else
    res.status(404).send('Not logged in');

}


exports.find = function(req, res){
  
  collection.find({}, function(e, docs){
    if( docs !== null ){
      res.json( docs );
    }else{
      res.status(404).send('Not found');
    }
  })
  
}


exports.login = function(req, res){
  
  req.session.user = {};
  req.session.user.name = "derek";
  res.send('logged in');
  
}

exports.logout = function(req, res){
  
  req.session.user = null;
  res.send('logged out');
  
}


exports.register = function(req, res){
  
  res.status(404).send('Work in progress');
  
}

