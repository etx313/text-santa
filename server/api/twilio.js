
/*
*   Twilio
*   derek.kuschel@lowe-ce.com
*/



var accountSid = 'AC7514f8dc7b5c98ccfd731e01dc3e0226',
    authToken = '3a7888c672472d471b52400903b04b86',
    
    twilio = require('twilio'),
    client = twilio(accountSid, authToken),
    monk = require('monk'),
    db =  monk('localhost:27017/santa'),
    messages = db.get('messages');
    
    natural = require('natural'),
    tokenizer = new natural.WordTokenizer();



var replies = [
              "Ho ho ho! I just showed your idea to Prancer, and he smiled. If you have anymore ideas, send them my way. 🎅 http://txtsanta.co",
              "By golly, be jolly. You just made Rudolph's nose light up. Send me more. 🎅 http://txtsanta.co",
              "Very nice, just like you. The elves can't wait to get to work on that one. 🎅 http://txtsanta.co",
              "Thank you. And I have an ask for you. Can you please leave carrots and chocolate milk this year? The Mrs. just loves that chocolate milk (wink wink). 🎅 http://txtsanta.co",
              "What a great idea! Perfect for such a good kid like you. 🎅 http://txtsanta.co",
              "I'm not sure on that one. Coal might be a better fit... Just kidding 🎅 http://txtsanta.co",
              "Good thinking. The elves have been asking to build that one. It has been added to the list! 🎅 http://txtsanta.co",
              "Oooh, that's a good one. Better hope you're on the nice list! 🎅 http://txtsanta.co",
              "How cool! I'll put my best elves on it! 🎅 http://txtsanta.co",
              "I hope Rudolph has been working out. That sleigh is going to be mighty heavy this year! 🎅 http://txtsanta.co",
              "I think that one is going to be making a lot of boys and girls jolly this year! 🎅 http://txtsanta.co",
              "Another great gift idea! Keep them coming&hellip; these elves work quickly! 🎅 http://txtsanta.co",
              "We're gonna need a bigger sleigh&hellip; 🎅 http://txtsanta.co",
              "Are you sure you don't just want some socks? We've got a lot of leftover socks. 🎅 http://txtsanta.co",
              "Hmm&hellip; Think that'll fit down a chimney? 🎅 http://txtsanta.co",
              "This looks great! The elves are going to be busy this year! 🎅 http://txtsanta.co",
              "Oh boy! Looks like someone is hoping they were extra good this year! 🎅 http://txtsanta.co",
              "The elves are excited about this one! 🎅 http://txtsanta.co",
              "Looks like I'll be delivering a lot of these this year&hellip; 🎅 http://txtsanta.co",
              "This one is going to a lot of good boys and girls this year. You have been good&hellip; right? 🎅 http://txtsanta.co",
              "It's a good thing we added that engineering program to Elf University! 🎅 http://txtsanta.co",
              "Tell you what&hellip; I'll trade you this for some milk and cookies. 🎅 http://txtsanta.co",
              "Better hope I can fit this under your tree! 🎅 http://txtsanta.co",
              "Great idea! The elves needed a new project! 🎅 http://txtsanta.co",
              "That looks like fun! Remember to save my number so you can text me anytime and add things to your list! 🎅 http://txtsanta.co",
              "Great! Be sure to let your friends and family know they can check out your list too. I'm sure lots of people want to get you presents this year! 🎅 http://txtsanta.co",
              "Thanks! We'll see if there's any more room in the sled! 🎅 http://txtsanta.co",
              "Ooh, I think Mrs. Claus might like one of those as well! Keep them coming! 🎅 http://txtsanta.co",
              "Added another one! Don't forget, you can share your list with loved ones so they know what to get you this year too! 🎅 http://txtsanta.co",
              "Ho ho ho! I had a feeling you might want one of those. 🎅 http://txtsanta.co"
              ];

var actions    = ['want','have','need','love','like'];
var articles   = ['a', 'an', 'some','the','lots of'];
var removelist = ['lots of','many','a lot of','tons of','lot of'];


function calcKeyword(msg){
 tokens = tokenizer.tokenize(msg);
 console.log("Finding token for message: ", msg);
 var stopinx = 0;
 var len = tokens.length;
 var actlen = actions.length;
 for( var i = 0; i < tokens.length ; i++){
   for(var j = 0; j < actions.length ; j++){
     if(tokens[i] == actions[j]){
       stopinx = i;
       break;
     }
   }
 }
 if(stopinx == 0){
   return msg;
 }
 else{
   for(var i = 0; i < articles.length; i++) 
    if(tokens[stopinx+1] == articles[i]){
       stopinx = stopinx + 1;
       break;
    }
    
   var result = '';
   for (var i = stopinx+1; i < tokens.length; i++)
      result += tokens[i] + ' ';
   
   
   for (var i = 0; i < removelist.length; i++){
     if(result.indexOf(removelist[i]) == 0){
       var offset  = removelist[i].length; 
       result = result.substr(offset);
     }
   }
   console.log(result);
   return result;
 }
 return msg; 
}


/* Module Exports */

exports.addTrigger = function(o){
  
  client.usage.triggers.create(o);
  
  console.log( "Added Trigger: ", o );
  
}

exports.recieve = function(req, res){

  // Verify Twilio post
  var verified = twilio.validateExpressRequest( req, authToken );
  console.log("Twilio Verified Post: ", verified);
  if( !verified ){
    res.status(401).send("Unauthorized");
    return;
  }
  
  
  if( req.body.MessageSid )
    client.messages( req.body.MessageSid ).get(function(error, sms) {
      
      if( error ){
        console.log( "Twilio Error: ", error );
        res.status(503).send("Service Unavailable");
        }
      
      var msg = JSON.parse( sms.nodeClientResponse.body );
      
      msg['key'] = calcKeyword(msg.body);
      
      messages.update({"phoneNumber":msg.from}, { $push:{ messages:msg } }, {upsert:true} ).complete(function(){
        // console.log("Upserted", arguments);
        });
      
      
      var resp = new twilio.TwimlResponse();
      resp.sms( replies[ Math.floor(Math.random()*replies.length) ] );
  
      res.type('text/xml');
      res.status(200);
      res.send(resp.toString());
      
      console.log("Sent response: ", resp.toString());
      
      
    });

    else
      console.log( "/api/twilio/recieve got a POST without a Message Sid", req.body );
  
  
  /*

  client.sms.messages.list( function( error, data ) {
  
    if( error ){
      console.log( "Twilio Error: ", error );
      res.status(503).send("Service Unavailable");
      }
      
    // console.log( "Data: ", data );
  
    var sms = data.sms_messages;
    var num = sms.length;
    for(var i= 0; i < 2; i++){
      if(sms[i]['to']=='+17542272682' && sms[i]['status']=='queued'){
       var ret = {};
       ret = sms[i];
       var key = calcKeyword(ret.body);
       ret['key'] = key;
       
       messages.update({"phoneNumber":ret.from}, { $push:{ messages:ret } }, {upsert:true} ).complete(function(){
         // console.log("Upserted", arguments);
       });
       
      }
    }
    total = num;
    inx  = Math.floor(Math.random()*6);
    
    var resp = new twilio.TwimlResponse();
    resp.say( replies[inx] );

    res.type('text/xml');
    res.send(resp.toString());
    
    console.log(resp.toString());
    
    
  });
  
  */


  
};










