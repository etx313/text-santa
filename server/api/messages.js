

/*
*   Messages
*/

var monk = require('monk'),
    db =  monk('localhost:27017/santa');


exports.find = function(req, res){
  
  var collection = db.get('messages');
  
  collection.findOne({"phoneNumber":req.params.phoneNumber}, function(e, doc){
    if( doc !== null ){
      res.json( doc.messages );
    }else{
      res.status(404).send('Not found');
    }
  })
  
}