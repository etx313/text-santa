
/**
 * Module dependencies.
 */

var express = require('express'),
    MongoStore = require('connect-mongo')(express),
    routes = require('./routes'),
    amazon = require('./api/amazon'),
    twilio = require('./api/twilio'),
    user   = require('./api/user'),
    messages = require('./api/messages'),
    http = require('http'),
    path = require('path'),
    app = express();
    

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.session({
  store: new MongoStore({
    url: 'mongodb://localhost:27017/santa'
  }),
  secret: '1234567890SANTAISREAL'
}));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*
*   API 
*/

app.all('/api/*', function(req, res, next){
  if (!req.get('Origin')) return next();
  // use "*" here to accept any origin
  // res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, POST');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
  res.set('Access-Control-Allow-Max-Age', 3600);
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});

app.all( '/api/apac/:product', amazon.product );
app.post('/api/twilio/recieve', twilio.recieve );
app.all( '/api/messages/:phoneNumber', messages.find );

app.all( '/api/user', user.index );
app.all( '/api/user/find', user.find );
app.post('/api/user/register', user.register );
app.all( '/api/user/login', user.login );
app.all( '/api/user/logout', user.logout );




// redirect all others to the index (HTML5 history)
app.all('/', function(req, res) {
  res.sendfile('index.html', { root: __dirname+'/public' });
});

app.all('*', function(req, res){
  res.send(404);
});




// Twilio setup
/*
twilio.addTrigger({
  triggerValue: "1",
  usageCategory: "sms",
  callbackUrl: app.get('port') == 3001 ? "http://beta.txtsanta.co/api/twilio/recieve":"http://txtsanta.co/api/twilio/recieve"
}, function(err, trigger) {
  process.stdout.write(trigger.sid);
});
*/




http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
