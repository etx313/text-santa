'use strict';

angular.module('santaApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate',
  'angulartics',
  'angulartics.google.analytics'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/views/home.html',
        controller: 'HomeController',
        showNav: false
      })
      .when('/wishlist/:phone', {
        templateUrl: '/views/wishlist.html',
        controller: 'WishlistController',
        showNav: true
      })
      .otherwise({
        redirectTo: '/'
      });
      
      // $locationProvider.html5Mode(true);
      
  })
  .constant('config', {
    'apiRoot':'http://localhost:3000/api'
/*     'apiRoot':'/api' */
  });
