'use strict';

angular.module('santaApp')
  .controller('WishlistController', function ($scope, $http, $routeParams, $timeout, messages ) {
    
    
    $scope.loaded = false;
    $scope.phoneNumber = $routeParams.phone;
    
    
    if( $scope.phoneNumber.length <= 10 )
      $scope.phoneNumber = "1"+$scope.phoneNumber;
      
    if( $scope.phoneNumber.length <= 11 )
      $scope.phoneNumber = "+"+$scope.phoneNumber;
    
    
    $scope.selectedMsg = -1;

    
    $scope.selectedMessage = function(i, key){
      if( $scope.selectedMsg != i )
        $scope.selectedMsg = i;
        else
        $scope.selectedMsg = -1;
        
      $scope.searchKey = key;
    }
    
    
    $scope.formatDate = function(str){
      var d = new Date(str);
      return d.getTime();
    }
    
    // Load messages from API
    messages.get($scope.phoneNumber).then(function( docs ){
      $scope.messages = docs;
      $scope.loaded = true;
    });
    
    
    
    /* Amazon */
    /*

    $scope.$watch('searchKey', function(){
      
      $scope.products = [];
      
      console.log("selectedMsg changed, search for products");
      
      Apac($scope.searchKey).then(function(data){
        $scope.products = data['ItemSearchResponse']['Items'][0]['Item'];
        
        console.log( $scope.products );
      });
      
    });
    
*/

    
    
  });












