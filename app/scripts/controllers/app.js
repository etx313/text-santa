'use strict';

angular.module('santaApp')
  .controller('AppController', function ($scope, $window, $route, $timeout) {
    
    $scope.navShow = false;
    
    $scope.nav = [
      {title:'How does it work?', link:'#'},
      {title:'Do not text', link:'#'},
      {title:'Register with Santa', link:'#'}
    ];
    
    $scope.$on('$routeChangeSuccess', function(next, current) { 
      
/*       $timeout(function(){ */
        $scope.navShow = !!$route.current.$$route.showNav;
/*       }, 100); */
      
    });
    
    
  });
