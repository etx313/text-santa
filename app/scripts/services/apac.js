'use strict';

angular.module('santaApp')
  .service('Apac', ['$http', '$q', function Apac($http, $q) {
    return function(p){
        
        var deferred = $q.defer();
        var serviceUri = 'http://localhost:3000/api/apac/'+p;
        
        $http.get(serviceUri).success(function(data) {
          deferred.resolve(data);
        }).error(function(){
          deferred.reject();
        });
        
        return deferred.promise;
      
    }
      
    
  }]);
  