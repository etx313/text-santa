'use strict';


angular.module('santaApp')
  .service('messages', ['$http', '$q', 'config', function Messages($http, $q, config) {
    return {
      get: function(phoneNumber){
          
          var deferred = $q.defer();
          var serviceUri = config.apiRoot+'/messages/'+phoneNumber;
          
          $http.get(serviceUri).success(function(data) {
            deferred.resolve(data);
          }).error(function(){
            deferred.reject();
          });
          
          return deferred.promise;
      },
      somethig: function(){}
      
      }
      
      
  }]);
  